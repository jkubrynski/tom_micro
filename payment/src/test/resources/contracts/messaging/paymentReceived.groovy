package contracts.messaging

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	label('paymentReceivedMessage')

	input {
		triggeredBy('sendPaymentReceivedMessage()')
	}

	outputMessage {
		sentTo('paymentEvents')
		headers {
			messagingContentType(applicationJson())
		}
		body(
			'id': '111',
			'eventType': 'PAID'
		)
	}
}