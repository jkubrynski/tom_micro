package contracts.rest

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	request {
		method(GET())
		url('/payments/111')
		headers {
			accept(applicationJsonUtf8())
		}
	}

	response {
		status(200)
		headers {
			contentType(applicationJsonUtf8())
		}
		body(
			'id': '111',
			'paymentStatus': 'OK'
		)
	}
}