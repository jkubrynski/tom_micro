package com.tomtom.payment;

import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMessageVerifier
abstract class MessagingBase {

	@Autowired
	PaymentFacade paymentFacade;

	void sendPaymentReceivedMessage() {
		paymentFacade.paymentReceived("111");
	}

}
