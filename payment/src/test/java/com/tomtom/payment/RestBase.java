package com.tomtom.payment;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.mockito.Mockito;

abstract class RestBase {

	@Before
	public void setUp() {
		PaymentFacade paymentFacade = Mockito.mock(PaymentFacade.class);

		Mockito.when(paymentFacade.getPaymentStatus("111"))
				.thenReturn(new Payment("111", PaymentStatus.OK));

		PaymentController paymentController = new PaymentController(paymentFacade);

		RestAssuredMockMvc.standaloneSetup(paymentController);
	}

	void sendPaymentReceivedMessage() {

	}
}
