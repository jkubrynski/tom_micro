package com.tomtom.payment;

class Payment {

	private final String id;
	private final PaymentStatus status;

	Payment(String id, PaymentStatus status) {
		this.id = id;
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public PaymentStatus getPaymentStatus() {
		return status;
	}
}
