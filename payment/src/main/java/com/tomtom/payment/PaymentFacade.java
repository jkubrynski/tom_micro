package com.tomtom.payment;

import java.lang.invoke.MethodHandles;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.MediaType;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
class PaymentFacade {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaymentsQueue paymentsQueue;

	PaymentFacade(PaymentsQueue paymentsQueue) {
		this.paymentsQueue = paymentsQueue;
	}

	Payment getPaymentStatus(String id) {
		LOG.info("Returning payment status [paymentId={}]", id);
		return new Payment(id, PaymentStatus.OK);
	}

	void paymentReceived(String id) {
		LOG.info("Received payment {}", id);
		PaymentEvent paymentEvent = new PaymentEvent(id);
		MessageHeaders messageHeaders = new MessageHeaders(
				Map.of(MessageHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE));

		paymentsQueue.paymentEvents().send(
				MessageBuilder.createMessage(paymentEvent, messageHeaders));
	}
}
