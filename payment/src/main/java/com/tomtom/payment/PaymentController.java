package com.tomtom.payment;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payments")
class PaymentController {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaymentFacade paymentFacade;

	PaymentController(PaymentFacade paymentFacade) {
		this.paymentFacade = paymentFacade;
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Payment getPayment(@PathVariable String id) {
		LOG.info("Checking payment status [paymentId={}]", id);
		return paymentFacade.getPaymentStatus(id);
	}

	@GetMapping(value = "/paid/{id}")
	void paid(@PathVariable String id) {
		paymentFacade.paymentReceived(id);
	}
}
