package com.tomtom.payment;

class PaymentEvent {

	private final String id;
	private final String eventType;

	PaymentEvent(String id) {
		this.id = id;
		this.eventType = "PAID";
	}

	public String getId() {
		return id;
	}

	public String getEventType() {
		return eventType;
	}
}
