package com.tomtom.cart;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("contracts")
@AutoConfigureStubRunner(ids = "com.tomtom:payment:+:stubs")
public class PaymentClientTest {

	@Autowired
	PaymentClient paymentClient;

	@Test
	public void shouldGetPaymentStatus() {
		PaymentResponse payment = paymentClient.getPayment("111");
		assertThat(payment.getPaymentStatus()).isEqualTo("OK");
		assertThat(payment.getId()).isEqualTo("111");
	}
}