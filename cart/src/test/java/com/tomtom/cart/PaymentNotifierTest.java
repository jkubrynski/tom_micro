package com.tomtom.cart;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.StubFinder;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("contracts")
@AutoConfigureStubRunner(ids = "com.tomtom:payment:+:stubs")
public class PaymentNotifierTest {

	@Autowired
	PaymentNotifier paymentNotifier;

	@Autowired
	StubFinder stubFinder;

	@Test
	public void shouldReceivePaymentEvent() {
		stubFinder.trigger("paymentReceivedMessage");

		PaymentEvent lastPaymentEvent = paymentNotifier.getLastPaymentEvent();
		assertThat(lastPaymentEvent.getId()).isEqualTo("111");
		assertThat(lastPaymentEvent.getEventType()).isEqualTo("PAID");
	}
}