package com.tomtom.cart;

import java.util.StringJoiner;

class PaymentResponse {

	private final String id;
	private final String paymentStatus;

	PaymentResponse(String id, String paymentStatus) {
		this.id = id;
		this.paymentStatus = paymentStatus;
	}

	public String getId() {
		return id;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", PaymentResponse.class.getSimpleName() + "[", "]")
				.add("id='" + id + "'")
				.add("paymentStatus='" + paymentStatus + "'")
				.toString();
	}
}
