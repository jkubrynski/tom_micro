package com.tomtom.cart;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/carts")
class CartController {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaymentService paymentService;

	CartController(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	@GetMapping("/{id}")
	String getCartStatus(@PathVariable String id) {
		LOG.info("Checking cart status [cartId={}]", id);
		return paymentService.getPaymentStatus(id);
	}
}
