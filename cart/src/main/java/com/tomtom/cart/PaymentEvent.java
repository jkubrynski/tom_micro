package com.tomtom.cart;

import java.util.StringJoiner;

class PaymentEvent {

	private String id;
	private String eventType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", PaymentEvent.class.getSimpleName() + "[", "]")
				.add("id='" + id + "'")
				.add("eventType='" + eventType + "'")
				.toString();
	}
}
