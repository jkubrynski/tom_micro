package com.tomtom.cart;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

@Service
class PaymentNotifier {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private PaymentEvent lastPaymentEvent;

	@StreamListener(PaymentsQueue.PAYMENT_EVENTS_TOPIC)
	void handlePaymentEvent(PaymentEvent paymentEvent) {
		LOG.info("Received payment event: {}", paymentEvent);
		this.lastPaymentEvent = paymentEvent;
	}

	PaymentEvent getLastPaymentEvent() {
		return lastPaymentEvent;
	}
}
