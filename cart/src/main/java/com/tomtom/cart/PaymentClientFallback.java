package com.tomtom.cart;

import org.springframework.stereotype.Component;

@Component
class PaymentClientFallback implements PaymentClient {

	@Override
	public PaymentResponse getPayment(String id) {
		return new PaymentResponse(id, "UNKNOWN");
	}
}
