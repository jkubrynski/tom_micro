package com.tomtom.cart;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
class PaymentService {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaymentClient paymentClient;

	PaymentService(PaymentClient paymentClient) {
		this.paymentClient = paymentClient;
	}

	String getPaymentStatus(String id) {
		LOG.info("Invoking payment client [cartId={}]", id);
		paymentClient.getPayment(id);
		return "PaymentStatus=" + paymentClient.getPayment(id);
	}
}
