package com.tomtom.cart;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

interface PaymentsQueue {

	String PAYMENT_EVENTS_TOPIC = "paymentEvents";

	@Input(PAYMENT_EVENTS_TOPIC)
	SubscribableChannel paymentEvents();
}
